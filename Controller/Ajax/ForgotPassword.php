<?php

namespace Hunters\PopUp\Controller\Ajax;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Framework\Escaper;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Model\AccountManagement;
use Magento\Framework\Exception\SecurityViolationException;

class ForgotPassword extends Action
{

    protected $_customerAccountManager;
    protected $_escaper;
    protected $_customerSession;
    protected $_redirectFactory;


    public function __construct(
        Context $context,
        Session $session,
        AccountManagementInterface $accountManagement,
        Escaper $escaper,
        PageFactory $resultPageFactory,
        RedirectFactory $redirectFactory
    )
    {
        $this->_customerSession = $session;
        $this->_customerAccountManager = $accountManagement;
        $this->_escaper = $escaper;
        $this->_redirectFactory = $redirectFactory->create();
        parent::__construct($context, $resultPageFactory);
    }

    public function execute()
    {
        $message = [
            'errors' => true,
            'message' => ''
        ];

        $post = $this->getRequest()->getPostValue();
        if (empty($post)) {
            return $this->_redirectFactory->setPath('/');
        }

        $email = $post['email'];
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        if ($email) {
            if (!\Zend_Validate::is($email, \Magento\Framework\Validator\EmailAddress::class)) {
                $this->_customerSession->setForgottenEmail($email);
                $message = [
                    'errors' => true,
                    'message' => __('Please correct the email address.')
                ];
                $resultJson->setData($message);
                return $resultJson;
            }

            try {
                $this->_customerAccountManager->initiatePasswordReset(
                    $email,
                    AccountManagement::EMAIL_RESET
                );
            } catch (SecurityViolationException $exception) {
                $message = [
                    'errors' => true,
                    'message' => $exception->getMessage()
                ];
                $resultJson->setData($message);
                return $resultJson;
            } catch (\Exception $e) {
                $message = [
                    'errors' => true,
                    'message' => __('We\'re unable to send the password reset email.')
                ];
                $resultJson->setData($message);
                return $resultJson;
            }
            $message = [
                'errors' => false,
                'message' =>$this->getSuccessMessage($email)
            ];

            $resultJson->setData($message);
            return $resultJson;
        } else {
            $message = [
                'errors' => true,
                'message' => __('Please enter your email.')
            ];
            $resultJson->setData($message);
            return $resultJson;
        }

        $resultJson->setData($message);
        return $resultJson;
    }

    protected function getSuccessMessage($email)
    {
        return __(
            'If there is an account associated with %1 you will receive an email with a link to reset your password.',
            $this->_escaper->escapeHtml($email)
        );
    }

}