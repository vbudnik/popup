<?php


namespace Hunters\PopUp\Controller\Ajax;

use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Customer\Helper\Address;
use Magento\Customer\Model\Account\Redirect;
use Magento\Framework\App\ObjectManager;
use Magento\Customer\Api\AccountManagementInterface;

class Registration extends \Magento\Framework\App\Action\Action
{

    protected $resultJsonFactory;

    protected $resultRawFactory;

    protected $session;

    protected $registration;

    protected $formKeyValidator;

    protected $customerExtractor;

    protected $accountManagement;

    protected $subscriberFactory;

    protected $customerUrl;

    protected $addressHelper;

    protected $storeManager;

    protected $accountRedirect;

    protected $scopeConfig;

    protected $cookieMetadataFactory;

    protected $cookieMetadataManager;

    protected $escaper;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Registration $customerRegistration,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Magento\Customer\Api\AccountManagementInterface $accountManagement,
        \Magento\Customer\Model\CustomerExtractor $customerExtractor,
        \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory,
        \Magento\Customer\Model\Url $customerUrl,
        \Magento\Framework\UrlInterface $urlModel,
        \Magento\Customer\Helper\Address $addressHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Account\Redirect $accountRedirect,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
        \Magento\Framework\Stdlib\Cookie\PhpCookieManager $cookieMetadataManager
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->session = $customerSession;
        $this->registration = $customerRegistration;
        $this->formKeyValidator = $formKeyValidator;
        $this->accountManagement = $accountManagement;
        $this->customerExtractor = $customerExtractor;
        $this->subscriberFactory = $subscriberFactory;
        $this->customerUrl = $customerUrl;
        $this->urlModel = $urlModel;
        $this->addressHelper = $addressHelper;
        $this->storeManager = $storeManager;
        $this->accountRedirect = $accountRedirect;
        $this->scopeConfig = $scopeConfig;
        $this->escaper = $escaper;
    }

    private function getCookieManager()
    {
        if (!$this->cookieMetadataManager) {
            $this->cookieMetadataManager = ObjectManager::getInstance()->get(
                \Magento\Framework\Stdlib\Cookie\PhpCookieManager::class
            );
        }
        return $this->cookieMetadataManager;
    }

    private function getCookieMetadataFactory()
    {
        if (!$this->cookieMetadataFactory) {
            $this->cookieMetadataFactory = ObjectManager::getInstance()->get(
                \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory::class
            );
        }
        return $this->cookieMetadataFactory;
    }

    public function execute()
    {
        $credentials = null;
        $httpBadRequestCode = 400;

        $resultRaw = $this->resultRawFactory->create();
        if ($this->getRequest()->getMethod() !== 'POST' || !$this->getRequest()->isXmlHttpRequest()) {
            return $resultRaw->setHttpResponseCode($httpBadRequestCode);
        }
        $formKeyValidation = true;
//        $formKeyValidation = $this->formKeyValidator->validate($this->getRequest());
        $response = [
            'errors' => false,
            'message' => __('Login successful.')
        ];
        if (!$this->registration->isAllowed()) {
            $response = [
                'errors' => true,
                'message' => __('Customer registration is already disabled.')
            ];
        } elseif (!$formKeyValidation) {
            $response = [
                'errors' => true,
                'message' => $this->getRequest()->getParam('password')
            ];
        } else {
            $this->session->regenerateId();
            try {

                $customer = $this->customerExtractor->extract('customer_account_create', $this->_request);
                $password = $this->getRequest()->getParam('password');
                $confirmation = $this->getRequest()->getParam('password_confirmation');
                $this->checkPasswordConfirmation($password, $confirmation);
                $customer = $this->accountManagement->createAccount($customer, $password);
//                file_put_contents(BP . '/var/log/popUP.log', "confirmation: \n", FILE_APPEND | LOCK_EX);
//                if ($this->getRequest()->getParam('is_subscribed', false)) {
//                    $this->subscriberFactory->create()->subscribeCustomerById($customer->getId());
//                }
                $this->_eventManager->dispatch(
                    'customer_register_success',
                    ['account_controller' => $this, 'customer' => $customer]
                );
                $confirmationStatus = $this->accountManagement->getConfirmationStatus($customer->getId());
                if ($confirmationStatus === AccountManagementInterface::ACCOUNT_CONFIRMATION_REQUIRED) {
                    $email = $this->customerUrl->getEmailConfirmationUrl($customer->getEmail());
                    $response = [
                        'errors' => true,
                        'message' => __(
                            'You must confirm your account. Please check your email for the confirmation link or <a href="%1">click here</a> for a new link.',
                            $email
                        )
                    ];
                } else {
                    $this->session->setCustomerDataAsLoggedIn($customer);
                    $response = [
                        'errors' => false,
                        'message' => "Success"
                    ];
                    $requestedRedirect = $this->accountRedirect->getRedirectCookie();
                    if (!$this->scopeConfig->getValue('customer/startup/redirect_dashboard') && $requestedRedirect) {
                        $response['redirectUrl'] = $this->_redirect->success($requestedRedirect);
                        $this->accountRedirect->clearRedirectCookie();
                    }
                }
                if ($this->getCookieManager()->getCookie('mage-cache-sessid')) {
                    $metadata = $this->getCookieMetadataFactory()->createCookieMetadata();
                    $metadata->setPath('/');
                    $this->getCookieManager()->deleteCookie('mage-cache-sessid', $metadata);
                }
            } catch (StateException $e) {
                $url = $this->_url->getUrl('customer/account/forgotpassword');
                $response = [
                    'errors' => true,
                    'message' => __(
                        'There is already an account with this email address. If you are sure that it is your email address, <a href="%1">click here</a> to get your password and access your account.',
                        $url
                    )
                ];
            } catch (InputException $e) {
                $response = [
                    'errors' => true,
                    'message' => $this->escaper->escapeHtml($e->getMessage())
                ];
            } catch (LocalizedException $e) {
                $response = [
                    'errors' => true,
                    'message' => $this->escaper->escapeHtml($e->getMessage())
                ];
            } catch (\Exception $e) {
                $response = [
                    'errors' => true,
                    'message' => __('We can\'t save the customer.')
                ];
            }
            $this->session->setCustomerFormData($this->getRequest()->getPostValue());
        }
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }

    protected function checkPasswordConfirmation($password, $confirmation)
    {
        if ($password != $confirmation) {
            throw new InputException(__('Please make sure your passwords match.'));
        }
    }

}