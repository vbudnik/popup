<?php

namespace Hunters\PopUp\Controller\Ajax;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\Session;

class Login extends Action
{
    protected $_redirectFactory;
    protected $_jsonFactory;
    protected $_customer;
    protected $_storeManager;
    protected $_customerSession;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        RedirectFactory $redirectFactory,
        JsonFactory $jsonFactory,
        CustomerRepositoryInterface $customer,
        StoreManagerInterface $storeManager,
        Session $session
    )
    {
        $this->_customerSession = $session;
        $this->_storeManager = $storeManager;
        $this->_customer = $customer;
        $this->_jsonFactory = $jsonFactory;
        $this->_redirectFactory = $redirectFactory->create();
        parent::__construct($context, $resultPageFactory);
    }

    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        if (empty($post)) {
            return $this->_redirectFactory->setPath('/');
        }
        $email = $post['email'];

        $storeID = $this->_storeManager->getStore()->getStoreId();
        try {
            $customerId = $this->_customer->get($email, $storeID)->getId();
        } catch (\Exception $e) {
            $customerId = null;
        }
        $data = [];
        if ($customerId) {
            $data = ['errors' => false];
        } else {
            $data = ['errors' => true];
        }
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($data);
        return $resultJson;
    }

}