<?php

namespace Hunters\PopUp\Block;

use Magento\Framework\View\Element\Template;

class PopUp extends \Magento\Framework\View\Element\Template
{

    protected $_httpContext;
    protected $_helper;
    protected $_url;
    protected $_allowPage = [
        'privacy-policy',
        'createpassword',
        'terms-of-service'
    ];


    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Http\Context $httpContext,
        \Hunters\PopUp\Helper\Data $helper,
        \Magento\Framework\UrlInterface $url,
        array $data = []
    ) {
        $this->_helper = $helper;
        $this->_httpContext = $httpContext;
        $this->_url = $url;
        parent::__construct($context, $data);
    }

    public function isLoginUser() {
        return $this->_httpContext
            ->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);
    }

    public function isEnable() {
        return (bool)$this->_helper->moduleEnabled();
    }

    public function allowPages() {
        $allowPages = $this->_allowPage;
        foreach ($allowPages as $allowPage) {
            if (strpos($this->_url->getCurrentUrl(), $allowPage)) {
                return true;
            }
        }
        return false;
    }

}